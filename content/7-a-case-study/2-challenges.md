# 7.2 Challenges

---

*written by Mary Croke, ‘13, and Sophia Rosenbaum, both NY City News Service*

---

This project presented significant challenges in sourcing characters to illustrate the stories we wanted to tell. Finding experts and advocates was no problem - there are plenty of those who work in offices in the city. But finding young people who had been in jail, who were in prison or had experience in the court system proved to be very difficult.

In general, teenagers can be unreliable and hard to reach. More than a few journalists thought they found a source, only to be stood up or not have phone calls returned. The journalists overcame this by showing up at family courts in different boroughs and meeting people outside who connected us to sources.

The other way we found compelling characters was through advocates who work with young people in the court system. At first, many of the advocates worried about confidentiality issues, but we were able to get in touch with sources this way, too. We entered sources with contact information on a shared Google document, and tracked when we corresponded so other students would know if they called someone when a colleague had just talked to a source. We also shared contact information with our colleagues if we found a source who would work for a different story in the package.

Throughout the project, we met as a group and sometimes thought, ‘Is this going to work? Should we abandon ship and pick a new project?’ But there was always at least one student who was encouraging and thought we were on to something worth reporting. It’s easy to feel bogged down or want to give up if things aren’t going well, but a lot of times, it just takes a little longer to get the story. Persistence is key in long-term group projects.

With a large group of people, it’s easy to get bogged down and not stay focused. Assigning a couple of people to lead the group is key. Also, once all the components of the project are complete, it’s good to have an overall editor look at all the pieces and make sure the tone is the same throughout and there isn’t any repetition.

---
