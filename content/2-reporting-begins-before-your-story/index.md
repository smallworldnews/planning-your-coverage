# 2. Reporting Begins Before Your Story Starts

---

Journalists are only as good as their sources. Your sources are people you go to for information about the story you are covering. The best journalists are cultivating sources all the time (for more on sources, see the Journalism lesson 1.3 “Selecting and Verifying Sources” in the StoryMaker Lessons) In a social-first newsroom this includes people whom you follow, people who follow you and the groups and threads you belong to on social platforms such as Facebook and YouTube.

As you begin to develop sources and an audience, do not wait until an event is scheduled or news breaks to begin your coverage of an issue of importance to you, your news organization and your audience. For example, issues like pensions for veterans or education. Make contacts and get involved in the discussion, online and off, about an issue before a related news event even happens. If you are informed on a topic when an event turns the issue into a news story, you can publish or post more quickly.

So how do you know what issues you should research before a news event happen? Journalists typically cover a variety of topics and story types. This is called general assignment. But, most specialize in a handful of topics to develop good sources and an expertise of their own. Most news organizations will encourage reporters to develop specialties in the areas the news organization most commonly covers. These categories of coverage are often called beats. Education is a beat. Politics is a beat. Sports and Finance are both beats. A beat can also be geographic -- a province, city or neighborhood.

Ideally, reporters working for the same outlet will have complementary beats that allow the organization to cover all topics that are essential to its editorial focus.  Good editors hire and assign specific reporters to cover important beats. Based on your interests, and the focus of the news organization, work with your editors and define one or two beats for yourself. In addition to sources, begin to collect information about your beat. Read up on your beat. Save online documents, bookmark important links and resources that you could post as part of your story.

Today more than ever the individual journalist is a “brand.” People go to one site for their news instead of another not because they like one publication overall more than the other but because they like the individual journalist. In a social-first newsroom the journalists share responsibility with the publication for bringing audience to the site. You need to actively work to build your own online audience, funneling them to your news site.

A social first newsroom should have a social editor. Reporters should work closely with the social editor to develop a strategy to build and engage audience. Study your “metrics” -- data about who is looking at and contributing to your stories, where and why. Adjust your coverage to grow your audience. Additionally, part of the reporter’s job in a social-first news environment is to aggregate and curate content provided by your audience. The social editor should help with that too.

Mobile devices and the Internet are primarily a visual news environment. You need to know how to shoot photos and videos. Video is the fastest growing type of content for online news. (see the module, ‘Producing Video’ for instructions on how to make videos and the ‘Photo for News’ module for instruction on photography). If you do not know how to shoot compelling photos or a good video story, learn. You and your news organization will need a strategy for verifying and posting content provided by your audience.

---
