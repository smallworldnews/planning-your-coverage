# 4. The Planned Event

### The biggest advantage of the planned event is that it is planned -- you know it’s coming up. This will allow you to do several things:

---

## Build interest in your audience

in the days and weeks before the event begins, get your audience revved up about the issue. Give them background information, “factoids” about the issue they may not have known, but will find interesting. If the event is a press conference, ask your audience for questions to ask the speaker. Encourage audience contribution to your coverage of the event.

---


## Promote your coverage of the event

Be sure to tell your audience when, where, and how you will be covering the event. You may wish to cover an event live and stream video to give your coverage a sense of immediacy and excite your audience.

---

## Pre-report

Learn as much as you can about the issue before the event. Collect content that you might use during your coverage, such as photos, maps, or links to additional sources.

---

## Plan your coverage through the entirety of the news cycle

You might think of your event coverage in three phases: before the event, during the event and after the event. This is the news cycle. How and what information you publish will change throughout the cycle. Use your knowledge of the event and your pre-reporting to keep it fresh. For example, your coverage prior to the event will be primarily promotion to build interest. Your coverage during the event will be about the event itself. Your coverage after the event might be analysis. Talks to experts about what was said. Curate your audience’s reaction. Address questions raised during the event. Fact-check what was presented. Discuss the future of the issue.

---
